require 'test_helper'

class LawAreaCodeManager::Test < ActiveSupport::TestCase
  test '법정동 모델 존재 여부' do
    assert LawAreaCodeManager::Models::LawAreaCode, 'check model'
  end

  test '주소를 기준으로 법정동 코드 반환' do
    code = LawAreaCodeManager::Models::LawAreaCode.search_lac('서울특별시 종로구 청운동', '서울특별시 종로구 청운동')
    assert code == '1111010100100000000', 'check code'
  end

  test '현재 fixture 전체 갯수 반환' do
    assert_equal 6, LawAreaCodeManager::Models::LawAreaCode.all.count, 'model count'
  end

  test '법정동 시군구 반환' do
    addr_text = LawAreaCodeManager::Models::LawAreaCode.search_si_gun_gu_by_lac(4889046021)
    assert_equal('경상남도 합천군', addr_text, '주소 비교')
  end

  test '법정동 시군구 반환된 주소명으로 코드 반환' do
    addr_code = LawAreaCodeManager::Models::LawAreaCode.search_si_gun_gu_code_by_lac(4889046021)
    assert_equal('4889000000', addr_code, '코드 비교')
  end

  test '법정동 시군구 반환2' do
    model = LawAreaCodeManager::Models::LawAreaCode.find_by(code: 4889046021)
    addr_text = model.parse_si_gun_gu
    assert_equal('경상남도 합천군', addr_text, '주소 비교')
  end
end
