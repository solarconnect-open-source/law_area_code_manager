module LawAreaCodeManager
  module Models
    class LawAreaCode < ActiveRecord::Base
      def parse_si_gun_gu
        return LawAreaCode.parse_si_gub_gu(self.addr)
      end

      class << self
        def parse_si_gub_gu(addr_text)
          si = addr_text.match(/(\S*시)/)
          gun = addr_text.match(/(\S*군)/)
          gu = addr_text.match(/(\S*구)/)

          unless gu.nil?
            end_index = addr_text.index(gu[0]) + gu[0].length
            return addr_text[0..end_index].strip
          end

          unless gun.nil?
            end_index = addr_text.index(gun[0]) + gun[0].length
            return addr_text[0..end_index].strip
          end

          unless si.nil?
            end_index = addr_text.index(si[0]) + si[0].length
            return addr_text[0..end_index].strip
          end

          return nil
        end

        def search_si_gun_gu_by_lac(code)
          lac_model = LawAreaCode.find_by_code(code)
          return LawAreaCode.parse_si_gub_gu(lac_model.addr)
        end

        def search_si_gun_gu_code_by_lac(code)
          addr_text = LawAreaCode.search_si_gun_gu_by_lac(code)
          model = LawAreaCode.find_by_addr(addr_text)
          return model.code
        end

        def search_lac(user_addr_text, addr_text)
          raise "LawAreaCode empty" if LawAreaCode.count == 0
          raise "LawAreaCode.search_lac param user_add_text" if user_addr_text.nil?
          raise "LawAreaCode.search_lac param addr_text" if addr_text.nil? || addr_text.length == 0

          result_addr = []

          # 정상적인 주소에 공백 기준
          addr_list = addr_text.split(' ')
          addr_list.each_index { |i|
            result_addr << addr_list[0..i].join(' ')
          }

          # 주소 뒤 배열부터 하나씩 제거하면서 code 를 찾는다.
          lac_model = nil
          result_addr.reverse.each do |item|
            lac_model = LawAreaCode.find_by_addr(item)
            unless lac_model.nil?
              break
            end
          end

          # 산이면 2, 일반이면 1
          # land = addr_text.include?('산') ? 2:1
          land = addr_text.match(/산(\s*)(\d+)/) == nil ? 1:2

          # 지번 (본번, 부번) 을 정규식을 통해 찾는다
          jibun_info = Hash.new
          jibun_regex = user_addr_text.match(/(?<bonbun>\d*)(\s*)-(\s*)(?<bubun>\d*)/)

          if jibun_regex.nil?
            jibun_regex = user_addr_text.match(/(?<bonbun>\d+)/)
            jibun_info['bonbun'] = jibun_regex.nil? ? nil : jibun_regex[:bonbun]
            jibun_info['bubun'] = nil
          else
            jibun_info['bonbun'] = jibun_regex[:bonbun]
            jibun_info['bubun'] = jibun_regex[:bubun]
          end

          # 법정지역코드 조합
          lac = "#{lac_model.code}#{land}#{'%04d' % jibun_info['bonbun'].to_i}#{'%04d' % jibun_info['bubun'].to_i}"

          if lac.length != 19
            raise 'lac 19 character error'
          end

          return lac
        end
      end
    end
  end
end
