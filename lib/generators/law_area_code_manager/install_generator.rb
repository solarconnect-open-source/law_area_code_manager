require 'rails/generators'
require 'rails/generators/migration'
require 'rails/generators/active_record'

module LawAreaCodeManager
  class InstallGenerator < Rails::Generators::Base
    include Rails::Generators::Migration
    def self.next_migration_number(dirname)
      ActiveRecord::Generators::Base.next_migration_number(dirname)
    end

    def table_name
      return 'law_area_codes'
    end

    source_root File.expand_path('../templates', __FILE__)

    desc 'Creates a LawAreaCode initializer.'

    def copy_law_area_code_migration
      migration_template('migration.template.erb', 'db/migrate/create_law_area_codes.rb')
    end

    def copy_law_area_code_model
      copy_file('law_area_code.erb', 'app/models/law_area_code.rb')
    end
  end
end