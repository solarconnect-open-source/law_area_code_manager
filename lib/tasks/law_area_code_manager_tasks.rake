require 'open-uri'

namespace(:law_area_code_manager) {
  desc '법정 지역 주소 코드 db에 저장하기'
  task :save_db => :environment do
    # ==========================
    # 데이터 다운로드
    # ==========================
    pp "데이터 다운로드"
    # 파일 Link
    text_file_link = 'http://www.code.go.kr/jsp/inc/codeFullDown.jsp?codeseId=%B9%FD%C1%A4%B5%BF%C4%DA%B5%E5%20%C0%FC%C3%BC%C0%DA%B7%E1'

    # zip 파일 열기
    file = open(text_file_link)

    # 파일이 일반 텍스트 파일인 경우
    if file.content_type.eql? 'text/plain'
      lac_text = file.read
    else # 파일이 zip 파일인 경우
      Zip::ZipFile.open(file.path) do |z|
        lac_text = z.read(z.first)
      end
    end

    # EUC-KR 인 경우
    raw_items = Iconv.iconv('UTF-8', 'EUC-KR', lac_text)[0].split(/\r\n/)

    # ==========================
    # 데이터 프로세싱
    # ==========================
    pp "데이터 프로세싱"
    processed_items = []
    raw_items.each_with_index do |item, index|
      data = item.match(/(?<code>.{10})\t(?<addr>[\W\w\d\D\s\S]*)\t(?<exist>\W*)/) # '4971025927      제주도 북제주군 조천읍 북촌리   폐지'
      processed_items << data unless data.nil? && index > 0
    end

    # ==========================
    # 데이터 저장
    # ==========================
    pp "데이터 저장"
    begin
      processed_items.each_with_index do |item, index|
        next if index == 0
        model = LawAreaCode.find_or_create_by(code: item[:code])
        if model.addr.blank?
          pp "processed_items #{index} saved !"
          model.addr = item[:addr]
          model.exist = item[:exist]
          model.save
        elsif model.addr != item[:addr] || model.exist != item[:exist]
          pp "processed_items #{index} updated !"
          model.addr = item[:addr]
          model.exist = item[:exist]
          model.save
        end
      end
    rescue => e
      pp e
    end

    pp 'done !'
  end
}