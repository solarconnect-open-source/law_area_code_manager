require 'law_area_code_manager/models/law_area_code'

module LawAreaCodeManager
  class Railtie < Rails::Railtie
    rake_tasks do
      load 'tasks/law_area_code_manager_tasks.rake'
    end
  end
end
