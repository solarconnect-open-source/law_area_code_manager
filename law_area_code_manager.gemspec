$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "law_area_code_manager/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "law_area_code_manager"
  s.version     = LawAreaCodeManager::VERSION
  s.authors     = ["ilsang yu"]
  s.email       = ["ilsang.yu@solarconnect.kr"]
  s.homepage    = "http://solarconnect.kr"
  s.summary     = "LawAreaCode."
  s.description = "LawAreaCode."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.0.2"

  s.add_development_dependency "sqlite3"
end
